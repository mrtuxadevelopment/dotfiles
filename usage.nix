{pkgs, ...}:
{
  # TODO: Use a hook so that it starts only *after* the shmem device is initialized
  systemd.user.services.scream-ivshmem = {
    enable = true;
    description = "Scream IVSHMEM";
    serviceConfig = {
      ExecStart =
        "${pkgs.scream}/bin/scream-ivshmem-pulse /dev/shm/scream";
      Restart = "always";
    };
    wantedBy = [ "multi-user.target" ];
    requires = [ "pulseaudio.service" ];
  };

  virtualisation = {
    sharedMemoryFiles = {
      scream = {
        user = "mrtuxa";
        group = "qemu-libvirtd";
        mode = "666";
      };
      looking-glass = {
        user = "mrtuxa";
        group = "qemu-libvirtd";
        mode = "666";
      };
    };
    libvirtd = {
      enable = true;
      qemu.ovmf.enable = true;
      qemu.runAsRoot = false;


      deviceACL = [
      	"/dev/vfio/vfio"      
        "/dev/vfio/17"
	"/dev/kvm"
	"/dev/shm/scream"
	"/dev/shm/looking-glass"
      ];
      onBoot = "ignore";
      onShutdown = "shutdown";

      clearEmulationCapabilities = false;
    };
    vfio = {
      enable = true;
      IOMMUType = "amd";
      devices = [ "10de:1381" "10de:0fbc" ];
      blacklistNvidia = true;
      disableEFIfb = false;
      ignoreMSRs = true;
      applyACSpatch = false;
    };
    hugepages = {
      enable = true;
      defaultPageSize = "1G";
      pageSize = "1G";
      numPages = 16;
    };
  };
}
